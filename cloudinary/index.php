<?php
    require "vendor/autoload.php";
    require "config.php";
    if(isset($_POST['submit'])){
        $public = $_POST['pub'];
        $file_up = $_FILES['file']['tmp_name'];

        \Cloudinary\Uploader::upload($file_up, array("public_id" => $public));
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cloudinary test</title>
</head>
<body>
    <form enctype="multipart/form-data" method="post">
        <input type="text" name="pub" placeholder="set your public_id">
        <?php echo cl_image_upload_tag('image_id'); ?>
        <input type="submit" name="submit" value="click here!">
    </form>

    <hr>
    <!-- <?php echo cl_image_tag('samples/imagecon-group') ?> -->
    
</body>
<script type="text/javascript" src="jquery.min.js"></script>
<script type="text/javascript" src="jquery.ui.widget.js"></script>
<script type="text/javascript" src="jquery.iframe-transport.js"></script>
<script type="text/javascript" src="jquery.fileupload.js"></script>
<script type="text/javascript" src="jquery.cloudinary.js"></script>
</html>